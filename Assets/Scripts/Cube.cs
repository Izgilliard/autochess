﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Cube : MonoBehaviour {
    [Header("Stats")]
    public bool IsRed;
    [SerializeField] private int maxHp;
    public int MaxHp {
        get => maxHp;
        set {
            maxHp = value;
            if (maxHp < 0) maxHp = 0;
        }
    }

    [SerializeField] private int curHp;
    public int CurHp {
        get => curHp;
        set {
            curHp = value;
            if (curHp < 0) curHp = 0;
            if (curHp > maxHp) curHp = maxHp;
        }
    }

    [SerializeField] private int damage;
    public int Damage {
        get => damage;
        set {
            damage = value;
            if (damage < 0) damage = 0;
        }
    }
    public float AttackSpeed = 10;

    [Header("Position")]
    public Game.Coord Pos;

    [Header("Gauge")]
    public RectTransform Gauge;
    public Image HpImg;
    public TextMeshProUGUI HpTx;
    public TextMeshProUGUI DamageTx;

    Game gameL;
    public void Init() {
        gameL = GameObject.FindGameObjectWithTag("Game").GetComponent<Game>();
        MeshRenderer mr = gameObject.GetComponent<MeshRenderer>();
        mr.material.color = (IsRed) ? Color.red : Color.blue;
        gameObject.name = (IsRed) ? "Red" : "Blue";
        transform.SetParent(gameL.CubeParent, false);
    }

    public void Reload(Cube otherCube) {
        gameObject.SetActive(true);
        int startMinHp = otherCube.damage * gameL.MinTurnCount;
        MaxHp = startMinHp + Random.Range(1, startMinHp * 2);
        ChangeHp(maxHp, true);
        DamageTx.text = $"Урон: {Damage}";
        Gauge.gameObject.SetActive(true);
    }

    public void ChangeHp(int val, bool setThis) {
        if (setThis) {
            CurHp = val;
        } else {
            CurHp += val;
        }

        HpTx.text = $"{CurHp}/{MaxHp}";
        HpImg.fillAmount = (float)CurHp / MaxHp;
        if (CurHp <= 0) Die();
    }

    public void Attack(Cube c) {
        if (!c) return;
        c.ChangeHp(-damage, false);

        startPos = transform.position;
        endPos = c.transform.position;
        StartAnim();

    }

    void Die() {
        gameObject.SetActive(false);
        gameL.ShowVictory(!IsRed);
    }

    public void ToPos(int x, int y) {
        Pos.X = x;
        Pos.Y = y;
        transform.localPosition = gameL.GetCellPos(x, y);
    }

    public void Move(int deltaX, int deltaY) {
        ToPos(Pos.X + deltaX, Pos.Y + deltaY);
    }

    private void Update() {
        Gauge.position = Camera.main.WorldToScreenPoint(transform.position);
        if (!IsAnim) return;
        DoAnim();
    }

    Vector3 startPos;
    Vector3 endPos;

    float startTime;
    float moveLength;
    bool IsAnim = false;
    bool IsGoFormard = false;

    void StartAnim() {
        startTime = Time.time;
        moveLength = Vector3.Distance(startPos, endPos);
        IsAnim = true;
        IsGoFormard = true;
    }

    void DoAnim() {
        float alreadyMove = (Time.time - startTime) * AttackSpeed;
        float progress = alreadyMove / moveLength;
        transform.position = Vector3.Lerp(startPos, endPos, progress);
        if (progress >= 1f) {
            if (IsGoFormard) {
                IsGoFormard = false;
                Vector3 markerBuffer = startPos;
                startPos = endPos;
                endPos = markerBuffer;
                startTime = Time.time;
            } else {
                IsAnim = false;
            }
        }
    }
}
