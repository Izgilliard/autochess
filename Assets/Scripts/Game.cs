﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game : MonoBehaviour {
    [System.Serializable]
    public class Coord {
        public int X;
        public int Y;
    }

    public enum GameState {
        Nothing,
        DoStep,
        DoAttack,
        ShowVictory
    }

    [Header("Battle field")]
    public Transform CellParent;
    public Transform CubeParent;
    public GameObject CellPrefab;
    public Coord FieldSize;
    public Vector2 CellSize;

    [Header("Fighters")]
    public Cube RedCube;
    public Cube BlueCube;

    [Header("Parametrs")]
    public int MinTurnCount;
    public GameState CurrentState;

    [Header("Times")]
    public float StepTime;
    public float AttackTime;
    public float ShowVictoryTime;

    [Header("UI")]
    public Button PlayBtn;
    public TextMeshProUGUI VictoryTx;

    void Start() {
        for (var i = 0; i < FieldSize.X; i++) {
            for (var j = 0; j < FieldSize.Y; j++) {
                GameObject cell = Instantiate(CellPrefab);
                cell.name = $"Cell {i} {j}";
                cell.transform.SetParent(CellParent, false);
                cell.transform.localPosition = GetCellPos(i, j);
            }
        }
        RedCube.Init();
        BlueCube.Init();
        ShowBattleField(false);
    }

    void ShowBattleField(bool isShow) {
        CellParent.gameObject.SetActive(isShow);
        PlayBtn.gameObject.SetActive(!isShow);
        VictoryTx.gameObject.SetActive(false);
        if (!isShow) SetState(GameState.Nothing);
    }

    int halfFieldSize => FieldSize.Y / 2;
    public void BeginFight() {
        RedCube.Reload(BlueCube);
        BlueCube.Reload(RedCube);
        RedCube.ToPos(Random.Range(0, FieldSize.X), Random.Range(0, halfFieldSize));
        BlueCube.ToPos(Random.Range(0, FieldSize.X), Random.Range(halfFieldSize + 1, FieldSize.Y));
        curCube = (Random.Range(0, 2) == 0) ? RedCube : BlueCube;
        ShowBattleField(true);
        DoTurn();
    }

    Cube curCube;
    Cube otherCube => (curCube == RedCube) ? BlueCube : RedCube;

    public void DoTurn() {
        bool needMove = CheckMove(curCube, otherCube);
        if (needMove) {
            SetState(GameState.DoStep);
            curCube.Move(deltaX, deltaY);
        } else {
            SetState(GameState.DoAttack);
            curCube.Attack(otherCube);
        }
        curCube = otherCube;
    }

    int deltaX = 0;
    int deltaY = 0;
    bool CheckMove(Cube cube, Cube otherCube) {
        bool isMove = false;
        deltaX = 0;
        deltaY = 0;
        if (Mathf.Abs(cube.Pos.X - otherCube.Pos.X) > 1) {
            deltaX = (curCube.Pos.X - otherCube.Pos.X < 0) ? 1 : -1;
            isMove = true;
        }
        if (Mathf.Abs(cube.Pos.Y - otherCube.Pos.Y) > 1) {
            deltaY = (curCube.Pos.Y - otherCube.Pos.Y < 0) ? 1 : -1;
            isMove = true;
        }
        return isMove;
    }

    public Vector3 GetCellPos(int i, int j) {
        return new Vector3(CellSize.x * i, 0, CellSize.y * j);
    }

    public void ShowVictory(bool isRedWinner) {
        VictoryTx.gameObject.SetActive(true);
        VictoryTx.text = (isRedWinner) ? "Оглушительная победа Красного" : "Убедительный успех Синего";
        VictoryTx.color = (isRedWinner) ? Color.red : Color.blue;
        SetState(GameState.ShowVictory);
        RedCube.Gauge.gameObject.SetActive(false);
        BlueCube.Gauge.gameObject.SetActive(false);
    }

    public void SetState(GameState gs) {
        CurrentState = gs;
        switch (gs) {
            case GameState.DoStep:
                delay = StepTime;
                break;
            case GameState.DoAttack:
                delay = AttackTime;
                break;
            case GameState.ShowVictory:
                delay = ShowVictoryTime;
                break;
        }
    }

    float delay;
    void Update() {
        RotateCamera();
        if (CurrentState == GameState.Nothing) return;
        delay -= Time.deltaTime;
        if (delay <= 0) {
            switch (CurrentState) {
                case GameState.DoAttack:
                case GameState.DoStep:
                    DoTurn();
                    break;
                case GameState.ShowVictory:
                    ShowBattleField(false);
                    break;
            }
        }
    }

    [Header("Camera Rotation")]
    public Vector2 CameraSpeed;
    public Transform CameraCenter;

    float cameraStepX = 0f;
    float cameraStepY = 0f;

    private void RotateCamera() {
        if (!Input.GetMouseButton(0)) return;
        cameraStepX += CameraSpeed.x * Input.GetAxis("Mouse X");
        cameraStepY -= CameraSpeed.y * Input.GetAxis("Mouse Y");
        if (cameraStepY < -90) cameraStepY = -90;
        if (cameraStepY > 0) cameraStepY = 0;
        CameraCenter.eulerAngles = new Vector3(cameraStepY, cameraStepX, 0f);
    }
}
